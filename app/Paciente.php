<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paciente extends Model
{
    use SoftDeletes;

    public $table = 'pacientes';

    protected $dates = [
        'nascimento',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const SEXO_RADIO = [
        'masculino' => 'Masculino',
        'feminino'  => 'Feminino',
    ];

    const AREA_SELECT = [
        'EAPIIS' => 'E.A.P.I.I.Sistemas',
        'EAPIC' => 'E.A.P.I.Civil',
        'EIIB' => 'Educacion I.I.B.',
        'EAPIM'  => 'E.A.P.I.Minas',
        'EAPIA'  => 'E.A.P.I.Agroind.',
        'EADMIN'  => 'E.P.Administracion',
        'CPG'  => 'C.Politica y G.',
        'EAPIADR'  => 'Ing. Agroecologia',
        'MVZ'  => 'Med.Veterinaria y Z.',
        'Administrativo' =>'Administrativo',
        'Externo' => 'Externo(Emergencia)'
    ];

    protected $fillable = [
        'nome',
        'dni',
        'codigo',
        'nascimento',
        'sexo',
        'email',
        'area',
        'urban',
        'cidade',
        'estado',
        'direccion',
        'created_at',
        'updated_at',
        'deleted_at',
        'observacoes',
        'telefono',
    ];

    public function atendimentos()
    {
        return $this->hasMany(Atendimento::class, 'paciente_id', 'id');
    }

    public function getNascimentoAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setNascimentoAttribute($value)
    {
        $this->attributes['nascimento'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}

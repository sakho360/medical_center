<?php

namespace App\Http\Controllers\Admin;

use App\Atendimento;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyAtendimentoRequest;
use App\Http\Requests\StoreAtendimentoRequest;
use App\Http\Requests\UpdateAtendimentoRequest;
use App\Paciente;
use App\Service;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AtendimentoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('atendimento_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $atendimentos = Atendimento::all();

        return view('admin.atendimentos.index', compact('atendimentos'));
    }

    public function create()
    {
        abort_if(Gate::denies('atendimento_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = Paciente::all()->pluck('nome', 'id')->prepend(trans('global.pleaseSelect'), '');
        $services = Service::all()->pluck('nome', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.atendimentos.create', compact('pacientes','services'));
    }

    public function store(StoreAtendimentoRequest $request)
    {
        $atendimento = Atendimento::create($request->all());

        return redirect()->route('admin.atendimentos.index');
    }

    public function edit(Atendimento $atendimento)
    {
        abort_if(Gate::denies('atendimento_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pacientes = Paciente::all()->pluck('nome', 'id')->prepend(trans('global.pleaseSelect'), '');
        $services = Service::all()->pluck('nome', 'id')->prepend(trans('global.pleaseSelect'), '');

        $atendimento->load('paciente');
        $atendimento->load('service');

        return view('admin.atendimentos.edit', compact('pacientes','atendimento','services'));
    }

    public function update(UpdateAtendimentoRequest $request, Atendimento $atendimento)
    {
        $atendimento->update($request->all());

        return redirect()->route('admin.atendimentos.index');
    }

    public function show(Atendimento $atendimento)
    {
        abort_if(Gate::denies('atendimento_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $atendimento->load('paciente');
        $atendimento->load('service');

        return view('admin.atendimentos.show', compact('atendimento'));
    }

    public function destroy(Atendimento $atendimento)
    {
        abort_if(Gate::denies('atendimento_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $atendimento->delete();

        return back();
    }

    public function massDestroy(MassDestroyAtendimentoRequest $request)
    {
        Atendimento::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

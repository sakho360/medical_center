<?php

use App\Service;
use App\Permissions;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    public function run()
    {
        $services = [
            [
                'id'    => 1,
                'nome' => 'Tópico',
            ],
            [
                'id'    => 2,
                'nome' => 'Med. General',
            ],
            [
                'id'    => 3,
                'nome' => 'Odontología',
            ],
            [
                'id'    => 4,
                'nome' => 'Psicología',
            ],
            [
                'id'    => 5,
                'nome' => 'Psicopedagogía',
            ],
        ];

        Service::insert($services);
    }
}
